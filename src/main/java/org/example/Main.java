package org.example;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[] students = {"Антонов", "Шевченко", "Бойко", "Шевченко", "Кравченко", "Савицький",
                "Антонов", "Шевченко", "Тетерів", "Мамченко"};
        List<String> studentsList = Arrays.asList(students); //один способ перевода из массива в список
        System.out.println(studentsList.toString());
        studentsList = new ArrayList<>();
        //второй способ перевода данных из массива в список
        for (String string : students) {
            studentsList.add(string);
        }
        System.out.println(studentsList.toString());

        Set<String> studentsSet = new TreeSet<>();
        for (String string : students) {
            studentsSet.add(string);
        }
        System.out.println(studentsSet.toString());

        Map<String, Integer> studentsMap = new HashMap<>();
        for (String string : students) {
            studentsMap.put(string, string.length());
        }
        System.out.println(studentsMap.toString());



    }
}